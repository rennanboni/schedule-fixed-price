package br.com.cea.transporte.batch.reader;

import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.cea.transporte.batch.institucional.model.Fornecedor;
import br.com.cea.transporte.batch.institucional.service.FornecedorService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class IntegrationItemReader implements ItemReader<Object> {

	@Autowired
	private FornecedorService service;

	private boolean batchJobState = false;

	@Override
	public Object read() throws Exception {
		log.info("Processando...");
		if (!batchJobState) {
			List<Fornecedor> list = this.service.consultar();
			list.forEach(lista -> System.out.println(lista));
			batchJobState = true;
		}
		return null;
	}

}
