package br.com.cea.transporte.batch.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "batchEntityManagerFactory"
					 , transactionManagerRef = "batchTransactionManager"
					 , basePackages = {	"br.com.cea.transporte.batch.config" }
)
public class BatchDataBaseConfig {

	@Primary
	@Bean(name = "batchDataSource")
	@ConfigurationProperties(prefix = "spring.batch.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "batchEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean batchEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("batchDataSource") DataSource dataSource) {
		
		return builder.dataSource(dataSource)
				      .packages("br.com.cea.transporte.batch.config")
				      .persistenceUnit("batch")
				      .build();
	}

	@Primary
	@Bean(name = "batchTransactionManager")
	public PlatformTransactionManager batchTransactionManager(
			@Qualifier("batchEntityManagerFactory") EntityManagerFactory batchEntityManagerFactory) {
		return new JpaTransactionManager(batchEntityManagerFactory);
	}

}
