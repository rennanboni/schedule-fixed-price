package br.com.cea.transporte.batch.institucional.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cea.transporte.batch.institucional.model.Fornecedor;
import br.com.cea.transporte.batch.institucional.repository.FornecedorRepository;

@Service
public class FornecedorService {
	
	@Autowired
	private FornecedorRepository repository;
	
	public List<Fornecedor> consultar() {
		return this.repository.findAll();
	}

}
