package br.com.cea.transporte.batch.institucional.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "SUPPLIER")
public class Fornecedor {

	@Id
	@GeneratedValue
	@Column(name = "SupplierID", nullable = false)
	private Long id;

	@Column(name = "SupplierTypeID")
	private Integer supplierTypeID;

	@Column(name = "SocietyID", length = 4)
	private String societyID;

	@Column(name = "RFC", length = 20)
	private String rfc;

	@Column(name = "SupplierDesc", length = 100)
	private String supplierDesc;

	@Column(name = "ContactName", length = 100)
	private String contactName;

	@Column(name = "ContactPhone", length = 20)
	private String contactPhone;

	@Column(name = "ContactMobile", length = 20)
	private String contactMobile;

	@Column(name = "ContactEmail", length = 50)
	private String contactEmail;
	
	@Column(name = "CountryCD", length = 3)
	private String countryCD;

	@Column(name = "BlockPayment", length = 3)
	private String blockPayment;

	@Column(name = "BlockSourcing")
	private Integer blockSourcing;

	@Column(name = "RegisterDate")
	private Date registerDate;

	@Column(name = "ProcessDate")
	private Date processDate;
}
