package br.com.cea.transporte.batch.institucional.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cea.transporte.batch.institucional.model.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {

}
