package br.com.cea.transporte.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class JobIntegrationXpartnerBatch1Application {

	public static void main(String[] args) {
		SpringApplication.run(JobIntegrationXpartnerBatch1Application.class, args);
	}
}
