package br.com.sysmap.schedule.loyalty.batch.repository;

import java.util.List;

import javax.sql.DataSource;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.service.ScheduleType;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OracleRepositoryTest {

	@Autowired
	OracleRepository repository;
	
	@Autowired
	DataSource dataSource;
	
	@Test
	public void getSchedulesPendingExecution() throws Exception {
		List<Schedule> schedules = repository.getSchedulesPendingExecution(ScheduleType.LOYALTY.getType(), dataSource);
		
		Assertions.assertThat(schedules).isNotNull();
	}
}
