package br.com.sysmap.schedule.loyalty.batch.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class JSONUtils {

	public static com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
	
	public static <T> T readValue(String value, Class<T> valueType) throws JsonParseException, JsonMappingException, IOException {
        return mapper.readValue(value, valueType);
    }

    public static String writeValue(Object value) throws JsonProcessingException {
        return mapper.writeValueAsString(value);
    }
	
}
