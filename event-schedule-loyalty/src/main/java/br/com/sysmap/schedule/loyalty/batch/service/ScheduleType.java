package br.com.sysmap.schedule.loyalty.batch.service;

public enum ScheduleType {
	
	SMS(1),
	ACTIVATE_BONUS(2),
	DEACTIVATE_BONUS(3),
	LOYALTY(4),
	CLOUSE_PROMOTION(5);
	
	private int type;
	
	private ScheduleType(int type) {
		this.type = type;
	}
	
	public int getType() {
		return this.getType();
	}
}
