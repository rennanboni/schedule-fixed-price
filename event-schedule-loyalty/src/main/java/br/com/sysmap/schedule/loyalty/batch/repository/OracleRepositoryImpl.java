package br.com.sysmap.schedule.loyalty.batch.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.model.ScheduleList;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

@Slf4j
@Repository
@SuppressWarnings("deprecation")
public class OracleRepositoryImpl implements OracleRepository {

	private static final Logger logger = LoggerFactory.getLogger("audit-log");

	@Override
	public void updateScheduleStatus(DataSource dataSource, Schedule schedule, String status) throws SQLException {

		Connection connection = dataSource.getConnection();

		CallableStatement call = connection.prepareCall("{call KOMS_SCHEDULE.PUPDATE ( P_CD_SCHEDULE => ?," + 
				"    P_CD_ACTIVITY => ?," + 
				"    P_DS_INDEX => ?," + 
				"    P_DT_SCHEDULE_START => ?, "+ 
				"    P_NM_STATUS => ?, "+ 
				"    P_DT_EXECUTION => ?)}");

		call.setInt(1, schedule.getCdSchedule());
		call.setInt(2, schedule.getCdActivity());
		call.setString(3, schedule.getDsIndex());
		call.setDate(4, new java.sql.Date(schedule.getDtSchedule().getTime()));
		call.setString(5, status);
		call.setDate(6, new java.sql.Date(new Date().getTime()));

		call.execute();
	}

	public List<Schedule> getSchedulesPendingExecution(int type, DataSource dataSource) throws Exception {
		Connection connection = dataSource.getConnection();

		CallableStatement call = connection.prepareCall("{call KOMS_SCHEDULE.PGET_SCHEDULES(P_DT_SCHEDULE => ?, P_CD_ACTIVITY => ?, P_SCHEDULES_WRAPPER => ?)}");

		call.setDate(1, new java.sql.Date(new Date().getTime()));
		call.setInt(2, type);
		call.registerOutParameter(3, OracleTypes.STRUCT, ScheduleList.SQL_TYPE);
		call.execute();

		Map<String, Class<?>> typeMap = connection.getTypeMap();
		typeMap.put(ScheduleList.SQL_TYPE, ScheduleList.class);
		typeMap.put(Schedule.SQL_TYPE, Schedule.class);
		
		ScheduleList scheduleList = (ScheduleList) call.getObject(3);

		call.close();
		
		List<Schedule> result = scheduleList != null ? scheduleList.getSchedules() : new ArrayList<Schedule>();

		return result;
	}
}
