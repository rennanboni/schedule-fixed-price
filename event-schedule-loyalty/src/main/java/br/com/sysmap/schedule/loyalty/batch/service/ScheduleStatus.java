package br.com.sysmap.schedule.loyalty.batch.service;

public enum ScheduleStatus {

	SUCCESS,
	RUNNING,
	ERROR
}
