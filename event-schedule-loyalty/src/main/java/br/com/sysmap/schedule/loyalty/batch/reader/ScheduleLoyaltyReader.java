package br.com.sysmap.schedule.loyalty.batch.reader;

import java.util.Collections;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.sysmap.schedule.loyalty.batch.config.OracleDataBaseConfig;
import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.service.ScheduleLoyaltyService;

//@Log4j2
public class ScheduleLoyaltyReader implements ItemReader<Schedule> {

	@Autowired
	OracleDataBaseConfig config;

	private static int nextIndex;
	private static List<Schedule> schedules;

	@Autowired
	ScheduleLoyaltyService service;

	@Override
	public synchronized Schedule read() throws Exception {

		if (schedules == null) {
			schedules = Collections.unmodifiableList(service.getSchedulesPendingExecution(config.dataSource()));
			nextIndex = 0;
		}

		Schedule next = null;
		if (nextIndex < schedules.size()) {
			next = schedules.get(nextIndex);
			nextIndex++;
		}

		return next;
	}

}
