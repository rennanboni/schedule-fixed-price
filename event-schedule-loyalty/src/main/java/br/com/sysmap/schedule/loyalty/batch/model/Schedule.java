package br.com.sysmap.schedule.loyalty.batch.model;

import java.io.StringWriter;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;

import br.com.sysmap.schedule.loyalty.batch.utils.IOUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Schedule implements SQLData {
	
	public static final String SQL_TYPE = "TP_SCHEDULE";
	
	private Integer cdSchedule;
	private Integer cdActivity;
	private String dsIndex;
	private String nmStatus;
	private String dsAttrValues;
	private Date dtSchedule;
	private Date dtExecution;
	private Date dtCreated;
	private Date dtUpdated;

	@Override
	public String getSQLTypeName() throws SQLException {
		return SQL_TYPE;
	}

	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		// IDs
		setCdSchedule(stream.readInt());
		setCdActivity(stream.readInt());
		
		// Data
		setDsIndex(stream.readString());
		setNmStatus(stream.readString());
		try {
			StringWriter text = new StringWriter();
			IOUtils.copy(stream.readClob().getAsciiStream(), text);
			setDsAttrValues(text.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Dates
		setDtSchedule(stream.readDate());
		setDtExecution(stream.readDate());
		setDtCreated(stream.readDate());
		setDtUpdated(stream.readDate());
	}

	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
		
		// IDs
		stream.writeInt(getCdSchedule());
		stream.writeInt(getCdActivity());
		
		// Data
		stream.writeString(getDsIndex());
		stream.writeString(getNmStatus());
		stream.writeString(getDsAttrValues());
		
		// Dates
		stream.writeDate(getDtSchedule() == null ? null : new java.sql.Date(getDtSchedule().getTime()));
		stream.writeDate(getDtExecution() == null ? null : new java.sql.Date(getDtExecution().getTime()));
		stream.writeDate(getDtCreated() == null ? null : new java.sql.Date(getDtCreated().getTime()));
		stream.writeDate(getDtUpdated() == null ? null : new java.sql.Date(getDtUpdated().getTime()));
	}
}
