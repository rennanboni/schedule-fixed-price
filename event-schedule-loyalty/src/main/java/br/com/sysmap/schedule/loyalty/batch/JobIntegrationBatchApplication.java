package br.com.sysmap.schedule.loyalty.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("br.com.sysmap.schedule.loyalty")
public class JobIntegrationBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobIntegrationBatchApplication.class, args);
	}
}
