
package br.com.sysmap.schedule.loyalty.batch.json.schedule.loyalty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "spcode",
    "sncode",
    "services",
    "status"
})
@Data
@NoArgsConstructor
@ToString
public class BonusService {

    @JsonProperty("spcode")
    public Integer spcode;
    @JsonProperty("sncode")
    public Integer sncode;
    @JsonProperty("services")
    public String services;
    @JsonProperty("status")
    public String status;

}
