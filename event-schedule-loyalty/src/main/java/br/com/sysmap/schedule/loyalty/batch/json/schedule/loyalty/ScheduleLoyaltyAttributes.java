
package br.com.sysmap.schedule.loyalty.batch.json.schedule.loyalty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bonusService",
    "coId",
    "promotion"
})
@Data
@NoArgsConstructor
@ToString
public class ScheduleLoyaltyAttributes {

    @JsonProperty("bonusService")
    public BonusService bonusService;
    @JsonProperty("coId")
    public String coId;
    @JsonProperty("promotion")
    public Promotion promotion;

}
