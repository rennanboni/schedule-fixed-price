package br.com.sysmap.schedule.loyalty.batch.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.processor.ScheduleLoyaltyProcessor;
import br.com.sysmap.schedule.loyalty.batch.reader.ScheduleLoyaltyReader;
import br.com.sysmap.schedule.loyalty.batch.writer.ScheduleLoyaltyItemWriter;

@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration extends DefaultBatchConfigurer{
	
		
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	Environment env;
	
	@Value("${max_threads}")
	private Integer max_threads;
	
	@Value("${chunk_size}")
	private Integer chunk_size;
	
	@Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

	@Override
    public void setDataSource(DataSource dataSource) {
        // override to do not set datasource even if a datasource exist.
        // initialize will use a Map based JobRepository (instead of database)
    }

	@Bean
	public Step processScheduleLoyaltyStep(@Qualifier("processScheduleLoyaltyItemReader") ItemReader<Schedule> reader,
				@Qualifier("processScheduleLoyaltyProcessor") ItemProcessor<Schedule, Schedule> processor,
				@Qualifier("processScheduleLoyaltyItemWriter") ItemWriter<Schedule> writer) {

		CompositeItemProcessor<Schedule, Schedule> compositeProcessor = new CompositeItemProcessor<Schedule, Schedule>();
		List<ItemProcessor<Schedule, Schedule>> itemProcessors = new ArrayList<ItemProcessor<Schedule, Schedule>>();
		itemProcessors.add(processor);
		compositeProcessor.setDelegates(itemProcessors);

		return stepBuilderFactory.get("processScheduleLoyaltyStep")
								 .<Schedule, Schedule>chunk(chunk_size).reader(reader)
								 .processor(compositeProcessor)
								 .writer(writer)
								 .taskExecutor(taskExecutor())
								 .throttleLimit(max_threads)
								 .build();
	}
		@Bean
	public Job job(@Qualifier("processScheduleLoyaltyStep") Step processScheduleLoyaltyStep) throws Exception {
		return jobBuilderFactory.get("JobScheduleLoyalty")
								.incrementer(new RunIdIncrementer())
								.start(processScheduleLoyaltyStep)				
								.build();
	}
	
	@Bean
	public TaskExecutor taskExecutor() {
		SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
		taskExecutor.setConcurrencyLimit(max_threads);
		return taskExecutor;
	}
	
	@Bean
	public ItemReader<Schedule> processScheduleLoyaltyItemReader() {
		return new ScheduleLoyaltyReader();
	}

	@Bean
	public ItemWriter<Schedule> processScheduleLoyaltyItemWriter() {
		return new ScheduleLoyaltyItemWriter();
	}
	
	@Bean
	public ItemProcessor<Schedule, Schedule> processScheduleLoyaltyProcessor() {
		return new ScheduleLoyaltyProcessor();
	}
	
	@PostConstruct
	public void setup() {
//		UnirestService.setup();
	}
}
