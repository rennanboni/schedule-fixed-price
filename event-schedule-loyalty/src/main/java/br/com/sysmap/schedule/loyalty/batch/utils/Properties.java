package br.com.sysmap.schedule.loyalty.batch.utils;

import org.springframework.core.env.Environment;

public enum Properties {
	BATCH_MAX_THREADS("max_threads"),
	BATCH_CHUNK_SIZE("chunk_size"),
	
	API_NEXTEL_ENDPOINT_API_BWS("api-nextel-endpoint-api-bws"),
	API_NEXTEL_ENDPOINT_API("api-nextel-endpoint-api"),
	API_NEXTEL_ENDPOINT_SINGLE("api-nextel-endpoint-single"),
	
	API_NEXTEL_GRANT_TYPE("api-nextel-grant-type"),
	API_NEXTEL_AUTHORIZATION("api-nextel-authorization"),
	API_NEXTEL_AUTHORIZATION_TTL("api-nextel-authentication-ttl"),
	;
	
	private String property;
	
	private Properties(String property) {
		this.property = property;
	}
	
	public String get(Environment env, String defaultValue) {
		return env.getProperty(this.property, defaultValue);
	}
	
	public String get(Environment env) {
		return this.get(env, null);
	}
}
