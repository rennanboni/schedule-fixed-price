
package br.com.sysmap.schedule.loyalty.batch.json.schedule.loyalty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "loyaltyMonths",
    "sncode",
    "spcode",
    "tmcode"
})
@Data
@NoArgsConstructor
@ToString
public class Promotion {

    @JsonProperty("loyaltyMonths")
    public Integer loyaltyMonths;
    @JsonProperty("sncode")
    public Integer sncode;
    @JsonProperty("spcode")
    public Integer spcode;
    @JsonProperty("tmcode")
    public Integer tmcode;

}
