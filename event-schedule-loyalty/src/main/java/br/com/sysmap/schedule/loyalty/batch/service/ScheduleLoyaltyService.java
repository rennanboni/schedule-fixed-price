package br.com.sysmap.schedule.loyalty.batch.service;

import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.repository.OracleRepository;
import br.com.sysmap.schedule.loyalty.batch.utils.Properties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ScheduleLoyaltyService {

	@Autowired
	Environment env;
	
	@Autowired
	private OracleRepository repository;
	
	@Transactional
	public List<Schedule> getSchedulesPendingExecution(DataSource dataSource) throws Exception {
		try {
			return repository.getSchedulesPendingExecution(ScheduleType.LOYALTY.getType(), dataSource);
		} catch (Exception e) {
			log.error("ERRO AO EXECUTAR A PROCEDURE:",e);
		}
		
		return null;
	}
	
	public void updateScheduleStatus(DataSource dataSource, Schedule schedule, String status) {
		try {
			repository.updateScheduleStatus(dataSource, schedule, status);
		} catch (Exception e) {
			log.error("ERRO AO EXECUTAR A PROCEDURE:",e);
		}
	}
	
	public OAuth2Request createOAuth2Request() {
    	OAuth2Request request  = new OAuth2Request();
    	{ // Request
    		request.setAuthorization(Properties.API_NEXTEL_AUTHORIZATION.get(env));
    		request.setGrantType(Properties.API_NEXTEL_GRANT_TYPE.get(env));
    	}
    	
    	return request;
    }
}
