package br.com.sysmap.schedule.loyalty.batch.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class IOUtils {

	public static final int EOF = -1;
	private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

	public static void copy(final InputStream input, final Writer output, final Charset inputEncoding) throws IOException {
		final InputStreamReader in = new InputStreamReader(input, Charsets.toCharset(inputEncoding));
		copy(in, output);
	}

	public static void copy(final InputStream input, final Writer output) throws IOException {
		copy(input, output, Charset.defaultCharset());
	}

	public static void copy(final InputStream input, final Writer output, final String inputEncoding) throws IOException {
		copy(input, output, Charsets.toCharset(inputEncoding));
	}

	public static void copy(final Reader input, final OutputStream output, final Charset outputEncoding) throws IOException {
		final OutputStreamWriter out = new OutputStreamWriter(output, Charsets.toCharset(outputEncoding));
		copy(input, out);
		// XXX Unless anyone is planning on rewriting OutputStreamWriter,
		// we have to flush here.
		out.flush();
	}

	public static long copy(final Reader input, final Appendable output) throws IOException {
		return copy(input, output, CharBuffer.allocate(DEFAULT_BUFFER_SIZE));
	}

	public static int copy(final InputStream input, final OutputStream output) throws IOException {
		final long count = copyLarge(input, output);
		if (count > Integer.MAX_VALUE) {
			return -1;
		}
		return (int) count;
	}

	public static long copy(final InputStream input, final OutputStream output, final int bufferSize) throws IOException {
		return copyLarge(input, output, new byte[bufferSize]);
	}

	public static long copyLarge(final InputStream input, final OutputStream output) throws IOException {
		return copy(input, output, DEFAULT_BUFFER_SIZE);
	}

	public static long copy(final Reader input, final Appendable output, final CharBuffer buffer) throws IOException {
		long count = 0;
		int n;
		while (EOF != (n = input.read(buffer))) {
			buffer.flip();
			output.append(buffer, 0, n);
			count += n;
		}
		return count;
	}

	public static long copyLarge(final InputStream input, final OutputStream output, final byte[] buffer) throws IOException {
		long count = 0;
		int n;
		while (EOF != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}
