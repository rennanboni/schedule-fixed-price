package br.com.sysmap.schedule.loyalty.batch.repository;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import br.com.sysmap.schedule.loyalty.batch.model.Schedule;

public interface OracleRepository {

	void updateScheduleStatus(DataSource dataSource, Schedule schedule, String status) throws SQLException;

	/**
	 * Search for schedule pending to be executed
	 * 
	 * @param type Schedule Type
	 * @param dataSource DataSource connection
	 * @returns List of schedules to be executed
	 * @throws Exception
	 */
	List<Schedule> getSchedulesPendingExecution(int type, DataSource dataSource) throws Exception;
}
