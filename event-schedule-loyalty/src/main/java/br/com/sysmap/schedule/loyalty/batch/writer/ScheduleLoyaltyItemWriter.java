package br.com.sysmap.schedule.loyalty.batch.writer;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.mashape.unirest.http.HttpResponse;

import br.com.sysmap.job.schedule.lib.service.BWSService;
import br.com.sysmap.job.schedule.lib.service.NextelService;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ContractDiscountsRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.response.ContractDiscountsResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.ContractChangeFeesRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response.ContractInformationResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.PromotionRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response.PromotionResponse;
import br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.CreateRARequest;
import br.com.sysmap.schedule.loyalty.batch.config.OracleDataBaseConfig;
import br.com.sysmap.schedule.loyalty.batch.json.schedule.loyalty.ScheduleLoyaltyAttributes;
import br.com.sysmap.schedule.loyalty.batch.model.Schedule;
import br.com.sysmap.schedule.loyalty.batch.service.ScheduleLoyaltyService;
import br.com.sysmap.schedule.loyalty.batch.service.ScheduleStatus;
import br.com.sysmap.schedule.loyalty.batch.utils.JSONUtils;
import br.com.sysmap.schedule.loyalty.batch.utils.Properties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScheduleLoyaltyItemWriter implements ItemWriter<Schedule> {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	ScheduleLoyaltyService service;

	@Autowired
	OracleDataBaseConfig config;
	
	@Autowired
	Environment env;

	@Override
	public void write(List<? extends Schedule> items) throws Exception {

		for (Schedule schedule : items) {
			try {

				service.updateScheduleStatus(config.dataSource(), schedule, ScheduleStatus.RUNNING.toString());
				
				ScheduleLoyaltyAttributes loyaltyAttributes = JSONUtils.readValue(schedule.getDsAttrValues(), ScheduleLoyaltyAttributes.class);
				
				/**************/
				/** CONTRACT **/
				/**************/
				HttpResponse<ContractInformationResponse> contractResponse = null;
				{
					contractResponse = BWSService.getContractInformation(loyaltyAttributes.getCoId(), Properties.API_NEXTEL_ENDPOINT_API_BWS.get(env));
				}
				
				/****************/
				/** PROMOTIONS **/
				/****************/
				HttpResponse<PromotionResponse> promotionResponse = null;
				{
					PromotionRequest request = new PromotionRequest();
					
					request.setPersonType(contractResponse.getBody().getCustomer().getType());
					request.setTmcode(loyaltyAttributes.getPromotion().getTmcode());
					
					{ // Services
						br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.Service();
						request.setServices(Arrays.asList(service));
						
						service.setSncode(loyaltyAttributes.getPromotion().getSncode());
						service.setSpcode(loyaltyAttributes.getPromotion().getSpcode());
					}
					
					promotionResponse = BWSService.getPromotion(request, Properties.API_NEXTEL_ENDPOINT_API_BWS.get(env));
				}
				
				
				/****************/
				/** Change Fee **/
				/****************/
				{
					ContractChangeFeesRequest request = new ContractChangeFeesRequest();
			    	{
			    		{ // Contract
			    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Contract contract = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Contract();
			    			
			    			contract.setId(loyaltyAttributes.getCoId());
			    			
			    			request.setContract(contract);
			    		}
			    		
			    		{ // Service
			    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service();
			    			
			    			
			    			{ // Fee
			    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Fee fee = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Fee();
			    				
			    				fee.setAmount(15F); // ????????
			    				fee.setIsAdvanced("Y");
			    				fee.setPeriod(loyaltyAttributes.getPromotion().getLoyaltyMonths());
			    				fee.setType("A");
			    				
			    				service.setFee(fee);
			    			}
			    			
			    			{ // Service_
			    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service_ service_ = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service_();
			    				
			    				service_.setSnCode(3424); // ??????
			    				
			    				service.setService(service_);
			    			}
			    			
			    			request.setService(Arrays.asList(service));
			    		}
			    	}
			    	
			    	BWSService.contractChangeFees(request, Properties.API_NEXTEL_ENDPOINT_API_BWS.get(env));
				}
				
				/***********************/
				/** Contract Discount **/
				/***********************/
				{
					ContractDiscountsRequest request = new ContractDiscountsRequest();
			    	{ // Request
			    		request.setId(loyaltyAttributes.getCoId());
			    		
			    		{ // Client
			    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Client client = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Client();
			    			request.setClient(client);
			    			
			    			client.setId("0");
			    			client.setCustCode(contractResponse.getBody().getCustomer().getCustCode());
			    			
			    			{ // Contract
			        			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Contract contract = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Contract();
			        			client.setContract(contract);
			        			
			        			contract.setId(loyaltyAttributes.getCoId());
			        			contract.setIsLoyalty("true");
			        			
			        			{ // Plan
			        				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Plan plan = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Plan();
			        				contract.setPlan(plan);
			        				
			        				plan.setAmount(contractResponse.getBody().getRateplan().getMonthlyFee());
			        				plan.setTmcode(contractResponse.getBody().getRateplan().getTmcode());
			        				
			        				{ // ChangeFee
			        					br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ChangeFee changeFee = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ChangeFee();
			        					plan.setChangeFee(changeFee);
			        					
			        					changeFee.setAmount(contractResponse.getBody().getRateplan().getMonthlyFee());
			        					changeFee.setCycle(loyaltyAttributes.getPromotion().getLoyaltyMonths());
			        					changeFee.setStartDate(dateFormat.format(new Date()));
			        					changeFee.setTotalPeriod(loyaltyAttributes.getPromotion().getLoyaltyMonths());
			        				}
			        			}
			        			
			        		}
			    			
			    			{ // Person
			    				if ("PF".equalsIgnoreCase(contractResponse.getBody().getCustomer().getType())) {
			    					br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Person person = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Person();
			    					client.setPerson(person);
			    					
			    					person.setCpf(contractResponse.getBody().getCustomer().getFederalTax());
			    				}
			    			}
			    			
			    			{ // LegalPerson
			    				if ("PJ".equalsIgnoreCase(contractResponse.getBody().getCustomer().getType())) {
			    					br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.LegalPerson legalPerson = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.LegalPerson();
			    					client.setLegalPerson(legalPerson);
			    					
			    					legalPerson.setCnpj(contractResponse.getBody().getCustomer().getFederalTax());
			    				}
			    			}
			    		}
			    		
			    		{ // Registry
			    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Registry registry = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Registry();
			    			request.setRegistry(registry);
			    			
			    			registry.setReferenceId(loyaltyAttributes.getCoId());
			    			registry.setUser("quarks");
			    			
			    			{ // Channel
			    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Channel channel = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Channel();
			    				registry.setChannel(channel);
			    				
			    				channel.setGroup("31");
			    				channel.setLocality("278");
			    			}
			    			
			    			{ // System
			    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.System system = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.System();
			    				registry.setSystem(system);
			    				
			    				system.setDescription("QUARKS");
			    				system.setId("12");
			    			}
			    		}
			    	}
			    	
			    	HttpResponse<ContractDiscountsResponse> response = BWSService.contractDiscounts(request, Properties.API_NEXTEL_ENDPOINT_SINGLE.get(env));
				}

				/***************/
				/** CREATE RA **/
				/***************/
				{
					CreateRARequest request = new CreateRARequest();
					{
						
						{ // Data
							br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.DataRequest data = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.DataRequest();
							request.setData(data);
							
							data.setCustomerCode(contractResponse.getBody().getCustomer().getCustCode());
							
							{ // RegisterAttendance
								br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.RegisterAttendance registerAttendance = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.RegisterAttendance();
								data.setRegisterAttendance(registerAttendance);
								
								registerAttendance.setDescription(String.format("Telefone %s: Fidelização da promoção A REGRA É CLARA foi renovada por mais 12 meses.", contractResponse.getBody().getMsisdn()));
								registerAttendance.setLogin("omsQuarks");
								registerAttendance.setReasonCode("omsQuarksRenovacao.renovarCopa");
							}
						}
						
						{ // Header
							br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.Header header = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.Header();
							request.setHeader(header);
							
							header.setClientId("omsQuarks");
						}
					}
					
					NextelService.createRA(request, service.createOAuth2Request(), Properties.API_NEXTEL_ENDPOINT_API.get(env));
				}

				service.updateScheduleStatus(config.dataSource(), schedule, ScheduleStatus.SUCCESS.toString());

			} catch (Exception e) {
				log.error("Error on ScheduleLoyaltyItemWriter", e);
				service.updateScheduleStatus(config.dataSource(), schedule, ScheduleStatus.ERROR.toString());
			}
		}

	}

}
