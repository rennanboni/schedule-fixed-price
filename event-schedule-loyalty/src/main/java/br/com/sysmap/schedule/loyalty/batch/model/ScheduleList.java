package br.com.sysmap.schedule.loyalty.batch.model;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import oracle.jdbc.driver.OracleConnection;
import oracle.sql.OracleSQLOutput;

@Getter
@Setter
public class ScheduleList implements SQLData {
	
	public static final String SQL_TYPE = "TP_SCHEDULES_WRAPPER";
	
	private List<Schedule> schedules;
	
	@Override
	public String getSQLTypeName() throws SQLException {
		return SQL_TYPE;
	}

	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		{ // Schedules
			schedules = Arrays.stream((Object[]) stream.readArray().getArray())
				.map(obj -> (Schedule) obj)
				.collect(Collectors.toList())
			;
		}
	}

	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
		final OracleSQLOutput out = (OracleSQLOutput) stream;
		OracleConnection conn = (OracleConnection) out.getSTRUCT().getJavaSqlConnection();
	    
		{ // Schedules
			if (getSchedules() == null) {
				setSchedules(Collections.emptyList());
			}
			
			out.writeArray(conn.createOracleArray("TP_SCHEDULES", getSchedules().toArray()));
		}
	}
}
