package br.com.sysmap.job.schedule.lib.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.response.OAuth2Response;

public class OAuth2ServiceTest {
	
	public static final String NEXTEL_ENDPOINT_HOST = "https://ms-apis-uat.nextel.com.br";
	
	@Before()
	public void setup() {
		UnirestService.setup();
	}

    @Test
    public void getOAuth2() throws UnirestException {
    	String baseUrl = String.format(NextelService.NEXTEL_CREATERA_PATH, OAuth2ServiceTest.NEXTEL_ENDPOINT_HOST);
    	
    	OAuth2Request request = OAuth2ServiceTest.createOAuth2Request();
    	
		HttpResponse<OAuth2Response> response = OAuth2Service.getOAuth2(request,  baseUrl);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getBody()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getAccessToken()).isNotNull();
    	Assertions.assertThat(response.getBody().getTokenType()).isNotNull();
    }
    
    public static OAuth2Request createOAuth2Request() {
    	OAuth2Request request  = new OAuth2Request();
    	{ // Request
    		request.setAuthorization("Basic bnN2OmhrNDU4ZHVl");
    		request.setGrantType("client_credentials");
    	}
    	
    	return request;
    }
}
