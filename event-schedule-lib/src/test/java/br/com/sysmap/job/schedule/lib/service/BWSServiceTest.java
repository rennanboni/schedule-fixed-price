package br.com.sysmap.job.schedule.lib.service;

import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ContractDiscountsRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.response.ContractDiscountsResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.response.ContractChangeFeesResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response.ContractInformationResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.PromotionRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response.PromotionResponse;

public class BWSServiceTest {
	
	public static final String BWS_ENDPOINT_HOST = "http://bws-u2.nextel.com.br";
	
	@Before()
	public void setup() {
		UnirestService.setup();
	}

    @Test
    public void testGetContractInformation() throws UnirestException {    	
    	HttpResponse<ContractInformationResponse> response = BWSService.getContractInformation(21286807, BWS_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getBody()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getRateplan()).isNotNull();
    	Assertions.assertThat(response.getBody().getRateplan().getTmcode()).isNotNull();
    	Assertions.assertThat(response.getBody().getRateplan().getMonthlyFee()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getCustomer()).isNotNull();
    	Assertions.assertThat(response.getBody().getCustomer().getId()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getMsisdn()).isNotNull();
    }
    
    @Test
    public void testGetPromotion() throws UnirestException {
    	PromotionRequest request = new PromotionRequest();
    	{
    		request.setPersonType("PF");
    		request.setTmcode(2298);
    		
    		{ // service
    			br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.Service();
    			
    			service.setSncode(3424);
    			service.setSpcode(2117);
    			
    			request.setServices(Arrays.asList(service));
    		}
    	}
    	
    	HttpResponse<PromotionResponse> response = BWSService.getPromotion(request, BWS_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getBody()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getPromotions()).isNotNull();
    	Assertions.assertThat(response.getBody().getPromotions().size()).isGreaterThan(0);
    	Assertions.assertThat(response.getBody().getPromotions().get(0).getTmcode()).isNotNull();
    	Assertions.assertThat(response.getBody().getPromotions().get(0).getSncode()).isNotNull();
    	Assertions.assertThat(response.getBody().getPromotions().get(0).getSpcode()).isNotNull();
    }
    
    @Test
    public void testContractChangeFees() throws UnirestException {
    	br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.ContractChangeFeesRequest request = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.ContractChangeFeesRequest();
    	{
    		{ // Contract
    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Contract contract = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Contract();
    			
    			contract.setId("21286192");
    			
    			request.setContract(contract);
    		}
    		
    		{ // Service
    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service();
    			
    			
    			{ // Fee
    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Fee fee = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Fee();
    				
    				fee.setAmount(15F);
    				fee.setIsAdvanced("Y");
    				fee.setPeriod(12);
    				fee.setType("A");
    				
    				service.setFee(fee);
    			}
    			
    			{ // Service_
    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service_ service_ = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.Service_();
    				
    				service_.setSnCode(3424);
    				
    				service.setService(service_);
    			}
    			
    			request.setService(Arrays.asList(service));
    		}

    	}
    	
    	HttpResponse<ContractChangeFeesResponse> response = BWSService.contractChangeFees(request, BWS_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getStatus()).isEqualTo(204);
    }
    
    
    @Test
    public void testContractDiscounts() throws UnirestException {
    	ContractDiscountsRequest request = new ContractDiscountsRequest();
    	{ // Request
    		request.setId("21286192");
    		
    		{ // Client
    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Client client = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Client();
    			
    			client.setId("0");
    			client.setCustCode("7.6002245.10");
    			
    			{ // Contract
        			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Contract contract = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Contract();
        			
        			contract.setId("21286192");
        			contract.setIsLoyalty("true");
        			
        			{ // Plan
        				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Plan plan = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Plan();
        				
        				plan.setAmount(427.51F);
        				plan.setTmcode(2295);
        				
        				{ // ChangeFee
        					br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ChangeFee changeFee = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ChangeFee();
        					
        					changeFee.setAmount(427.51F);
        					changeFee.setCycle(12);
        					changeFee.setStartDate("2019-04-05");
        					changeFee.setTotalPeriod(12);
        					
        					plan.setChangeFee(changeFee);
        				}
        				
        				contract.setPlan(plan);
        			}
        			
        			client.setContract(contract);
        		}
    			
    			{ // Person
    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Person person = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Person();
    				
    				person.setCpf("21238402038");
    				
    				client.setPerson(person);
    			}
    			
    			request.setClient(client);
    		}
    		
    		{ // Registry
    			br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Registry registry = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Registry();
    			
    			registry.setReferenceId("21286192");
    			registry.setUser("quarks");
    			
    			{ // Channel
    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Channel channel = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.Channel();
    				
    				channel.setGroup("31");
    				channel.setLocality("278");
    				
    				registry.setChannel(channel);
    			}
    			
    			{ // System
    				br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.System system = new br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.System();
    				
    				system.setDescription("QUARKS");
    				system.setId("12");
    				
    				registry.setSystem(system);
    			}
    			
    			request.setRegistry(registry);
    		}
    	}
    	
    	HttpResponse<ContractDiscountsResponse> response = BWSService.contractDiscounts(request, BWS_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getStatus()).isEqualTo(204);
    }
    
}
