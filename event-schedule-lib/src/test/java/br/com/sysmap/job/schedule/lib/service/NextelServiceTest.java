package br.com.sysmap.job.schedule.lib.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.CreateRARequest;
import br.com.sysmap.job.schedule.lib.service.json.nextel.createra.response.CreateRAResponse;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;

public class NextelServiceTest {
	
	public static final String NEXTEL_ENDPOINT_HOST = "https://ms-apis-uat.nextel.com.br";
	
	@Before()
	public void setup() {
		UnirestService.setup();
	}

    @Test
    public void testCreateRA() throws UnirestException {
    	// OAuth2Request
    	OAuth2Request oauth2Request = OAuth2ServiceTest.createOAuth2Request();
    	
    	// CreateRARequest
    	CreateRARequest request = new CreateRARequest();
    	{
    		
    		{ // Data
    			br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.DataRequest data = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.DataRequest();
    			request.setData(data);
    			
    			data.setCustomerCode("7.6001848.10");
    			
    			{ // RegisterAttendance
    				br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.RegisterAttendance registerAttendance = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.RegisterAttendance();
    				data.setRegisterAttendance(registerAttendance);
    				
    				registerAttendance.setDescription("Telefone 5511940039042: Fidelização da promoção A REGRA É CLARA foi renovada por mais 12 meses.");
    				registerAttendance.setLogin("omsQuarks");
    				registerAttendance.setReasonCode("omsQuarksRenovacao.renovarCopa");
    			}
    		}
    		
    		{ // Header
    			br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.Header header = new br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.Header();
    			request.setHeader(header);
    			
    			header.setClientId("omsQuarks");
    		}
    	}
    	
		HttpResponse<CreateRAResponse> response = NextelService.createRA(request, oauth2Request, NEXTEL_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    	Assertions.assertThat(response.getBody()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getDataResponse()).isNotNull();
    	Assertions.assertThat(response.getBody().getDataResponse().getRegisterAttendance()).isNotNull();
    	
    	Assertions.assertThat(response.getBody().getHeader()).isNotNull();
    }
    
}
