package br.com.sysmap.job.schedule.lib.service;

import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.PCRFActivateServiceRequest;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.response.PCRFActivateServiceResponse;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.deactivate.service.request.PCRFDeactivateServiceRequest;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.deactivate.service.response.PCRFDeactivateServiceResponse;

public class PCRFServiceTest {
	
	public static final String NEXTEL_ENDPOINT_HOST = "https://ms-apis-uat.nextel.com.br";
//	public static final String NEXTEL_ENDPOINT_HOST = "https://189.51.177.73";
	
	@Before()
	public void setup() {
		UnirestService.setupWithIgnoreSSL();
	}

    @Test
    public void testActivateService() throws UnirestException {
    	// OAuth2Request
    	OAuth2Request oauth2Request = OAuth2ServiceTest.createOAuth2Request();
    	
    	// PCRFActivateServiceRequest
    	PCRFActivateServiceRequest request = new PCRFActivateServiceRequest();
    	{
    		{ // Subscriber
    			br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Subscriber subscriber = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Subscriber();
    			request.setSubscriber(subscriber);
    			
    			subscriber.setImsi("724390999171258");
    		}
    		
    		{ // Services
    			br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Service();
    			request.setServices(Arrays.asList(service));
    			
    			service.setName("BONUS_TRATO_E_TRATO_1GB");
    			
    			{ // Expiration
    				br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Expiration expiration = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Expiration();
    				service.setExpiration(expiration);
    				
    				expiration.setDate("2020&04&01&02&47&32");
    			}
    		}
    	}
    	
		HttpResponse<PCRFActivateServiceResponse> response = PCRFService.activateService(request, oauth2Request, NEXTEL_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    }
    

    @Test
    public void testActivateService_shouldThrowExpcetion() {
    	// OAuth2Request
    	OAuth2Request oauth2Request = OAuth2ServiceTest.createOAuth2Request();
    	
    	// PCRFActivateServiceRequest
    	PCRFActivateServiceRequest request = new PCRFActivateServiceRequest();
    	{
    		{ // Subscriber
    			br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Subscriber subscriber = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Subscriber();
    			request.setSubscriber(subscriber);
    			
    			subscriber.setImsi("724390999171258");
    		}
    		
    		{ // Services
    			br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Service service = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Service();
    			request.setServices(Arrays.asList(service));
    			
    			service.setName("BONUS_TRATO_E_TRATO_1GB");
    			
    			{ // Expiration
    				br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Expiration expiration = new br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.Expiration();
    				service.setExpiration(expiration);
    				
    				expiration.setDate("2019-04-07T00:00:00Z"); // Invalid data format
    			}
    		}
    	}
    			
		Assertions.assertThatThrownBy(() -> PCRFService.activateService(request, oauth2Request, NEXTEL_ENDPOINT_HOST))
			.isInstanceOf(UnirestException.class);
    }
 
    @Test
    public void testDeactivateService() throws UnirestException {
    	// OAuth2Request
    	OAuth2Request oauth2Request = OAuth2ServiceTest.createOAuth2Request();
    	
    	// PCRFDeactivateServiceRequest
    	PCRFDeactivateServiceRequest request = new PCRFDeactivateServiceRequest();
    	{
    		request.setName("BONUS_RECORRENTE_1GB");
    		request.setImsi("724390000009932");
    	}
    	
		HttpResponse<PCRFDeactivateServiceResponse> response = PCRFService.deactivateService(request, oauth2Request, NEXTEL_ENDPOINT_HOST);
    	
    	Assertions.assertThat(response).isNotNull();
    }
}
