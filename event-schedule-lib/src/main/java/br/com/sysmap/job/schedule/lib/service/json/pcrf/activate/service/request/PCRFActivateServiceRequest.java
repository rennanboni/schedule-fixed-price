
package br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "services",
    "subscriber"
})
@Data
@NoArgsConstructor
@ToString
public class PCRFActivateServiceRequest {

    @JsonProperty("services")
    public List<Service> services = null;
    @JsonProperty("subscriber")
    public Subscriber subscriber;
}
