package br.com.sysmap.job.schedule.lib.service;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.mashape.unirest.http.Unirest;

import br.com.sysmap.job.schedule.lib.config.UnirestJacksonObjectMapper;

public class UnirestService {
	
	public static void setup() {
		Unirest.setObjectMapper(new UnirestJacksonObjectMapper());
	}
	
	public static void setup(boolean ignoreSSL) {
		if (ignoreSSL) {
			UnirestService.setupWithIgnoreSSL();
		} else {
			UnirestService.setup();
		}
	}
	
	public static void setupWithIgnoreSSL() {
		UnirestService.setup();
		
		try {
			SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();
			
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			CloseableHttpClient httpclient = HttpClients.custom()
					.setSSLSocketFactory(sslsf)
					.build();
			Unirest.setHttpClient(httpclient);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
