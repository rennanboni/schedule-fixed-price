package br.com.sysmap.job.schedule.lib.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.body.RequestBodyEntity;

import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request.ContractDiscountsRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.response.ContractDiscountsResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request.ContractChangeFeesRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.response.ContractChangeFeesResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response.ContractInformationResponse;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request.PromotionRequest;
import br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response.PromotionResponse;

public class BWSService extends BaseService {

    public static final String BWS_CONTRACT_INFORMATION_PATH = "%s/contract/v1/%s/information";
    public static final String BWS_PROMOTION_PATH = "%s/promotions/v1/loyalty/bonus";
    public static final String BWS_CONTRACT_CHANGE_FEES_PATH = "%s/contractFee/contracts/fees/v1";
    public static final String BWS_CONTRACT_DISCOUNTS_PATH = "%s/contractDiscount/contracts/discounts/V1";
    
    public static HttpResponse<ContractInformationResponse> getContractInformation(String contractId, String host) throws UnirestException {
    	String url = BWSService.getContractInformationUrl(contractId, host);
    	
    	GetRequest request = Unirest.get(url).headers(BaseService.DEFAULT_JSON_HEADER);
    	HttpResponse<ContractInformationResponse> response = request.asObject(ContractInformationResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }

	public static HttpResponse<ContractInformationResponse> getContractInformation(long contractId, String host) throws UnirestException {
    	return BWSService.getContractInformation(String.valueOf(contractId), host);
    }
    
    public static HttpResponse<PromotionResponse> getPromotion(PromotionRequest body, String host) throws UnirestException {
    	String url = BWSService.getPromotionUrl(host);
    	
    	RequestBodyEntity request = Unirest.post(url)
    	  .headers(BaseService.DEFAULT_JSON_HEADER)
    	  .body(body);
    	HttpResponse<PromotionResponse> response = request.asObject(PromotionResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    public static HttpResponse<ContractChangeFeesResponse> contractChangeFees(ContractChangeFeesRequest body, String host) throws UnirestException {
    	String url = BWSService.getContractChangeFeesUrl(host);
    	
    	RequestBodyEntity request = Unirest.post(url)
    	    	  .headers(BaseService.DEFAULT_JSON_HEADER)
    	    	  .body(body);
    	HttpResponse<ContractChangeFeesResponse> response = request.asObject(ContractChangeFeesResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    public static HttpResponse<ContractDiscountsResponse> contractDiscounts(ContractDiscountsRequest body, String host) throws UnirestException {
    	String url = BWSService.getContractDiscountsUrl(host);
    	
    	RequestBodyEntity request = Unirest.post(url)
    	    	  .headers(BaseService.DEFAULT_JSON_HEADER)
    	    	  .body(body);
    	HttpResponse<ContractDiscountsResponse> response = request.asObject(ContractDiscountsResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    public static String getContractInformationUrl(long contractId, String host) {
    	return String.format(BWS_CONTRACT_INFORMATION_PATH, host, contractId);
    }
    
    public static String getContractInformationUrl(String contractId, String host) {
    	return String.format(BWS_CONTRACT_INFORMATION_PATH, host, contractId);
    }
    
    public static String getPromotionUrl(String host) {
    	return String.format(BWS_PROMOTION_PATH, host);
    }
    
    public static String getContractChangeFeesUrl(String host) {
    	return String.format(BWS_CONTRACT_CHANGE_FEES_PATH, host);
    }
    
    public static String getContractDiscountsUrl(String host) {
    	return String.format(BWS_CONTRACT_DISCOUNTS_PATH, host);
    }
}