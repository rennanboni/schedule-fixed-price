
package br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "activDate",
    "deactivDate",
    "effectiveDate",
    "expirationDate",
    "tmcode",
    "sncode",
    "spcode",
    "smsTemplates",
    "loyaltyMonths",
    "loyaltyBonuses"
})
@Data
@NoArgsConstructor
@ToString
public class Promotion extends BaseResponse {

    @JsonProperty("id")
    public String id;
    @JsonProperty("name")
    public String name;
    @JsonProperty("activDate")
    public String activDate;
    @JsonProperty("deactivDate")
    public String deactivDate;
    @JsonProperty("effectiveDate")
    public String effectiveDate;
    @JsonProperty("expirationDate")
    public String expirationDate;
    @JsonProperty("tmcode")
    public Integer tmcode;
    @JsonProperty("sncode")
    public Integer sncode;
    @JsonProperty("spcode")
    public Integer spcode;
    @JsonProperty("smsTemplates")
    public List<SmsTemplate> smsTemplates = null;
    @JsonProperty("loyaltyMonths")
    public Integer loyaltyMonths;
    @JsonProperty("loyaltyBonuses")
    public List<LoyaltyBonuse> loyaltyBonuses = null;
}
