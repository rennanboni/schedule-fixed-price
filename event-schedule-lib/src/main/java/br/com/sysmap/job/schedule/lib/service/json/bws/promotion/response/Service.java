
package br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "spcode",
    "sncode",
    "serviceName",
    "status"
})
@Data
@NoArgsConstructor
@ToString
public class Service extends BaseResponse {

    @JsonProperty("spcode")
    public Integer spcode;
    @JsonProperty("sncode")
    public Integer sncode;
    @JsonProperty("serviceName")
    public String serviceName;
    @JsonProperty("status")
    public String status;
}
