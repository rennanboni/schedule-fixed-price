package br.com.sysmap.job.schedule.lib.service;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;

import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.response.OAuth2Response;

public class OAuth2Service {

	public static final String HEADER_AUTHORIZATION = "authorization";
	
	public static final String NEXTEL_OAUTH2_PATH = "%s/oauth2/token";
	public static final String HEADER_AUTHORIZATION_VALUE = "%s %s";
    
    public static HttpResponse<OAuth2Response> getOAuth2(OAuth2Request body, String baseUrl) throws UnirestException {
    	String url = OAuth2Service.getOAuth2Url(baseUrl);
    	Map<String, String> headers = new HashMap<String, String>(BaseService.DEFAULT_JSON_HEADER);
    	{
    		headers.put(HEADER_AUTHORIZATION, body.getAuthorization());
    	}
    	
    	RequestBodyEntity request = Unirest.post(url)
    	    	  .headers(headers)
    	    	  .body(body);
    	HttpResponse<OAuth2Response> response = request.asObject(OAuth2Response.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    
    public static String getOAuth2Url(String baseUrl) {
    	return String.format(NEXTEL_OAUTH2_PATH, baseUrl);
    }

	public static String createAuthorizationHeader(HttpResponse<OAuth2Response> oauth2Response) {
		return String.format(HEADER_AUTHORIZATION_VALUE, oauth2Response.getBody().getTokenType(), oauth2Response.getBody().getAccessToken());
	}
}