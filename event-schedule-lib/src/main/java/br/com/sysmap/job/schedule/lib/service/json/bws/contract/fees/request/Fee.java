
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "amount",
    "isAdvanced",
    "period",
    "type"
})
@Data
@NoArgsConstructor
@ToString
public class Fee {

    @JsonProperty("amount")
    public Float amount;
    @JsonProperty("isAdvanced")
    public String isAdvanced;
    @JsonProperty("period")
    public Integer period;
    @JsonProperty("type")
    public String type;
}
