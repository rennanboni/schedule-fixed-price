
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "amount",
    "changeFee",
    "tmcode"
})
@Data
@NoArgsConstructor
@ToString
public class Plan {

    @JsonProperty("amount")
    public Float amount;
    @JsonProperty("changeFee")
    public ChangeFee changeFee;
    @JsonProperty("tmcode")
    public Integer tmcode;
}
