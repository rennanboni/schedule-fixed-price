
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "amount",
    "cycle",
    "startDate",
    "totalPeriod"
})
@Data
@NoArgsConstructor
@ToString
public class ChangeFee {

    @JsonProperty("amount")
    public Float amount;
    @JsonProperty("cycle")
    public Integer cycle;
    @JsonProperty("startDate")
    public String startDate;
    @JsonProperty("totalPeriod")
    public Integer totalPeriod;

}
