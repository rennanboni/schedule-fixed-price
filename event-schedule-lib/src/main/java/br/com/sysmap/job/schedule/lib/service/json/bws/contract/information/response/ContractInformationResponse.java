
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "rateplan",
    "activationDate",
    "monthlyFee",
    "customer",
    "imsi",
    "msisdn"
})
@Data
@NoArgsConstructor
@ToString
public class ContractInformationResponse extends BaseResponse {

    @JsonProperty("status")
    public String status;
    @JsonProperty("rateplan")
    public Rateplan rateplan;
    @JsonProperty("activationDate")
    public String activationDate;
    @JsonProperty("monthlyFee")
    public Float monthlyFee;
    @JsonProperty("customer")
    public Customer customer;
    @JsonProperty("imsi")
    public String imsi;
    @JsonProperty("msisdn")
    public String msisdn;

}
