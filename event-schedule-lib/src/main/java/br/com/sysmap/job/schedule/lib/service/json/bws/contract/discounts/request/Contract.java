
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "isLoyalty",
    "plan"
})
@Data
@NoArgsConstructor
@ToString
public class Contract {

    @JsonProperty("id")
    public String id;
    @JsonProperty("isLoyalty")
    public String isLoyalty;
    @JsonProperty("plan")
    public Plan plan;
}
