
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tmcode",
    "monthlyFee",
    "servicePackage"
})
@Data
@NoArgsConstructor
@ToString
public class Rateplan extends BaseResponse {

    @JsonProperty("tmcode")
    public Integer tmcode;
    @JsonProperty("monthlyFee")
    public Float monthlyFee;
    @JsonProperty("servicePackage")
    public List<ServicePackage> servicePackage = null;

}
