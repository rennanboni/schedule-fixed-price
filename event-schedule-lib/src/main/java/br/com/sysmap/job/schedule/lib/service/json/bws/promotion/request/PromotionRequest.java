
package br.com.sysmap.job.schedule.lib.service.json.bws.promotion.request;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "personType",
    "tmcode",
    "services"
})
@Data
@NoArgsConstructor
@ToString
public class PromotionRequest {

    @JsonProperty("personType")
    public String personType;
    @JsonProperty("tmcode")
    public Integer tmcode;
    @JsonProperty("services")
    public List<Service> services = null;

}
