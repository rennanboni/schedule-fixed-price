package br.com.sysmap.job.schedule.lib.service;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.response.OAuth2Response;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request.PCRFActivateServiceRequest;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.response.PCRFActivateServiceResponse;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.deactivate.service.request.PCRFDeactivateServiceRequest;
import br.com.sysmap.job.schedule.lib.service.json.pcrf.deactivate.service.response.PCRFDeactivateServiceResponse;

public class PCRFService {

    public static final String NEXTEL_ACTIVATE_SERVICE_PATH = "%s/provisioning/data/V1/service";
    public static final String NEXTEL_DEACTIVATE_SERVICE_PATH = "%s/provisioning/data/V1/service/%s/imsi/%s";
    
    public static HttpResponse<PCRFActivateServiceResponse> activateService(PCRFActivateServiceRequest body, OAuth2Request oauth2Body, String host) throws UnirestException {
    	String url = PCRFService.getActivateServiceUrl(host);
    	
    	// OAuth2
    	HttpResponse<OAuth2Response> oauth2Response = OAuth2Service.getOAuth2(oauth2Body, url);
    	
    	// Headers
    	Map<String, String> headers = new HashMap<String, String>(BaseService.DEFAULT_JSON_HEADER);
    	{
    		headers.put(OAuth2Service.HEADER_AUTHORIZATION, OAuth2Service.createAuthorizationHeader(oauth2Response));
    	}
    	
    	RequestBodyEntity request = Unirest.post(url)
    			.headers(headers)
    			.body(body);
    	HttpResponse<PCRFActivateServiceResponse> response = request.asObject(PCRFActivateServiceResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    public static HttpResponse<PCRFDeactivateServiceResponse> deactivateService(PCRFDeactivateServiceRequest body, OAuth2Request oauth2Request, String host) throws UnirestException {
    	String url = PCRFService.getDeactivateServiceUrl(body.getName(), body.getImsi(), host);
    	
    	// OAuth2
    	HttpResponse<OAuth2Response> oauth2Response = OAuth2Service.getOAuth2(oauth2Request, url);
    	
    	// Headers
    	Map<String, String> headers = new HashMap<String, String>(BaseService.DEFAULT_JSON_HEADER);
    	{
    		headers.put(OAuth2Service.HEADER_AUTHORIZATION, OAuth2Service.createAuthorizationHeader(oauth2Response));
    	}
    	
    	HttpRequestWithBody request = Unirest.delete(url)
    			.headers(headers);
    	HttpResponse<PCRFDeactivateServiceResponse> response = request.asObject(PCRFDeactivateServiceResponse.class);
    	
    	return response;
    }
    
    
    public static String getActivateServiceUrl(String host) {
    	return String.format(NEXTEL_ACTIVATE_SERVICE_PATH, host);
    }
    
    public static String getDeactivateServiceUrl(String name, String imsi, String host) {
    	return String.format(NEXTEL_DEACTIVATE_SERVICE_PATH, host, name, imsi);
    }
}