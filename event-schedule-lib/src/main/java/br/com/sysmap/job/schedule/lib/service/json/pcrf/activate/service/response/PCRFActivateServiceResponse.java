package br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@ToString
public class PCRFActivateServiceResponse extends BaseResponse {

}
