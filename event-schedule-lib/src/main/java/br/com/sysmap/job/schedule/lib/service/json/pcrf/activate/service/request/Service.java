
package br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "expiration",
    "name"
})
@Data
@NoArgsConstructor
@ToString
public class Service {

    @JsonProperty("expiration")
    public Expiration expiration;
    @JsonProperty("name")
    public String name;
}
