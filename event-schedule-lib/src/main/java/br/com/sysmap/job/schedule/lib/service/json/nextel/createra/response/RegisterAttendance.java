
package br.com.sysmap.job.schedule.lib.service.json.nextel.createra.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "protocolNumber",
    "number"
})
@Data
@NoArgsConstructor
@ToString
public class RegisterAttendance extends BaseResponse {

    @JsonProperty("protocolNumber")
    public String protocolNumber;
    @JsonProperty("number")
    public String number;

}
