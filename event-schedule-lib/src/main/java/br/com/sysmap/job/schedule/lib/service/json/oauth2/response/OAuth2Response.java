package br.com.sysmap.job.schedule.lib.service.json.oauth2.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"token_type",
	"access_token",
	"expires_in"
})
@Data
@NoArgsConstructor
@ToString
public class OAuth2Response extends BaseResponse {

	@JsonProperty("token_type")
	public String tokenType;
	
	@JsonProperty("access_token")
	public String accessToken;
	
	@JsonProperty("expires_in")
	public Integer expiresIn;

}