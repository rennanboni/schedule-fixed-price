
package br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "promotions"
})
@Data
@NoArgsConstructor
@ToString
public class PromotionResponse extends BaseResponse {

    @JsonProperty("promotions")
    public List<Promotion> promotions = null;
}
