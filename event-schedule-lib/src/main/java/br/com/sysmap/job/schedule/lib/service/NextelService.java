package br.com.sysmap.job.schedule.lib.service;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;

import br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request.CreateRARequest;
import br.com.sysmap.job.schedule.lib.service.json.nextel.createra.response.CreateRAResponse;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.request.OAuth2Request;
import br.com.sysmap.job.schedule.lib.service.json.oauth2.response.OAuth2Response;

public class NextelService {

    public static final String NEXTEL_CREATERA_PATH = "%s/CreateRegisterAttendance/V1";
    
    public static HttpResponse<CreateRAResponse> createRA(CreateRARequest body, OAuth2Request oauth2Body, String host) throws UnirestException {
    	String url = NextelService.getCreateRAUrl(host);
    	
    	// OAuth2
    	HttpResponse<OAuth2Response> oauth2Response = OAuth2Service.getOAuth2(oauth2Body, url);
    	
    	// Headers
    	Map<String, String> headers = new HashMap<String, String>(BaseService.DEFAULT_JSON_HEADER);
    	{
    		headers.put(OAuth2Service.HEADER_AUTHORIZATION, OAuth2Service.createAuthorizationHeader(oauth2Response));
    	}
    	
    	RequestBodyEntity request = Unirest.post(url)
    			.headers(headers)
    			.body(body);
    	HttpResponse<CreateRAResponse> response = request.asObject(CreateRAResponse.class);
    	
    	BaseService.checkHttpStatus(request, response);
    	
    	return response;
    }
    
    
    public static String getCreateRAUrl(String host) {
    	return String.format(NEXTEL_CREATERA_PATH, host);
    }
}