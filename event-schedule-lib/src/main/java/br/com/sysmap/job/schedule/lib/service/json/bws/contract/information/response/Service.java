
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sncode",
    "frequency",
    "charge",
    "status",
    "subscription"
})
@Data
@NoArgsConstructor
@ToString
public class Service extends BaseResponse {

    @JsonProperty("sncode")
    public Integer sncode;
    @JsonProperty("frequency")
    public String frequency;
    @JsonProperty("charge")
    public Charge charge;
    @JsonProperty("status")
    public String status;
    @JsonProperty("subscription")
    public Boolean subscription;

}
