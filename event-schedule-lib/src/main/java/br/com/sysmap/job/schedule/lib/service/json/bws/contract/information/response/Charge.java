
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.information.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "accessFee",
    "subscript",
    "event"
})
@Data
@NoArgsConstructor
@ToString
public class Charge extends BaseResponse {

    @JsonProperty("type")
    public String type;
    @JsonProperty("accessFee")
    public AccessFee accessFee;
    @JsonProperty("subscript")
    public Subscript subscript;
    @JsonProperty("event")
    public Event event;

}
