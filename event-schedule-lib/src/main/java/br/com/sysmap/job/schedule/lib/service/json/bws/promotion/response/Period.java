
package br.com.sysmap.job.schedule.lib.service.json.bws.promotion.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.sysmap.job.schedule.lib.service.json.base.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "qty",
    "type"
})
@Data
@NoArgsConstructor
@ToString
public class Period extends BaseResponse {

    @JsonProperty("qty")
    public Integer qty;
    @JsonProperty("type")
    public String type;
}
