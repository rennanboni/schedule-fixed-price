package br.com.sysmap.job.schedule.lib.service;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.body.RequestBodyEntity;

import br.com.sysmap.job.schedule.lib.service.utils.JSONUtils;

public class BaseService {
	
	public static String GENERAL_ERROR_MSG_REQUEST = "Error calling {\"URL\":\"%s\",\"METHOD\":\"%s\",\"BODY\":%s} response: {\"STATUSCODE\":\"%s\",\"STATUSTEXT\":\"%s\",\"BODY\":%s}";

    public static final Map<String, String> DEFAULT_JSON_HEADER = new HashMap<String, String>(){{
    	put("accept", "application/json");
    	put("content-type", "application/json");
    }};
    
    public void checkHttpStatus(HttpResponse<?> response, Object request, String url) throws UnirestException {
    	if (Response.Status.Family.familyOf(response.getStatus()) != Response.Status.Family.SUCCESSFUL) {
//    		throw new UnirestException(String.format("", args));
    	}
    }

    public static void checkHttpStatus(HttpRequest request, HttpResponse<?> response) throws UnirestException {
    	if (Response.Status.Family.familyOf(response.getStatus()) != Response.Status.Family.SUCCESSFUL) {
    		
    		Object responseBody = null;
    		try {
    			responseBody = JSONUtils.writeValue(response.getBody());
    		} catch(Exception e) {
    			responseBody = response.getBody();
    		}
    		
    		String msg = String.format(GENERAL_ERROR_MSG_REQUEST, 
    				request.getUrl(),
    				request.getHttpMethod(),
    				request.getBody(),
    				response.getStatus(),
    				response.getStatusText(),
    				responseBody);
    		
    		throw new UnirestException(msg);
    	}
	}
    
    public static void checkHttpStatus(RequestBodyEntity request, HttpResponse<?> response) throws UnirestException {
    	if (Response.Status.Family.familyOf(response.getStatus()) != Response.Status.Family.SUCCESSFUL) {
    		
    		Object responseBody = null;
    		try {
    			responseBody = JSONUtils.writeValue(response.getBody());
    		} catch(Exception e) {
    			responseBody = response.getBody();
    		}
    		
    		String msg = String.format(GENERAL_ERROR_MSG_REQUEST, 
    				request.getHttpRequest().getUrl(),
    				request.getHttpRequest().getHttpMethod(),
    				request.getBody(),
    				response.getStatus(),
    				response.getStatusText(),
    				responseBody);
    		
    		throw new UnirestException(msg);
    	}
	}
}
