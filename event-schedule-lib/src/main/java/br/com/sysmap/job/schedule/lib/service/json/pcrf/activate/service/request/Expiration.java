
package br.com.sysmap.job.schedule.lib.service.json.pcrf.activate.service.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date"
})
@Data
@NoArgsConstructor
@ToString
public class Expiration {

    @JsonProperty("date")
    public String date;
}
