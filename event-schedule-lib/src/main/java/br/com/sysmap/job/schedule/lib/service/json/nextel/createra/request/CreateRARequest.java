
package br.com.sysmap.job.schedule.lib.service.json.nextel.createra.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "header"
})
@Data
@NoArgsConstructor
@ToString
public class CreateRARequest {

    @JsonProperty("data")
    public DataRequest data;
    @JsonProperty("header")
    public Header header;

}
