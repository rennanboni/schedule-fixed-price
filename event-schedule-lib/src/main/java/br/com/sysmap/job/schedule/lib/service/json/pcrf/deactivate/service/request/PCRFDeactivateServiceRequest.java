
package br.com.sysmap.job.schedule.lib.service.json.pcrf.deactivate.service.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class PCRFDeactivateServiceRequest {

    private String name;
    private String imsi;
}