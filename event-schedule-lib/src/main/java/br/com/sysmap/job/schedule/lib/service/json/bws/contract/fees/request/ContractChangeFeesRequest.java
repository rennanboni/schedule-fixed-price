
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.fees.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "contract",
    "service"
})
@Data
@NoArgsConstructor
@ToString
public class ContractChangeFeesRequest {

    @JsonProperty("contract")
    public Contract contract;
    @JsonProperty("service")
    public List<Service> service = null;
}
