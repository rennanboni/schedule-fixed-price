
package br.com.sysmap.job.schedule.lib.service.json.bws.contract.discounts.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "group",
    "locality"
})
@Data
@NoArgsConstructor
@ToString
public class Channel {

    @JsonProperty("group")
    public String group;
    @JsonProperty("locality")
    public String locality;
}
