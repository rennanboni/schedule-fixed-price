
package br.com.sysmap.job.schedule.lib.service.json.oauth2.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "grant_type"
})
@Data
@NoArgsConstructor
@ToString
public class OAuth2Request {

    @JsonProperty("grant_type")
    public String grantType;
    
    public String authorization;
}
